function cdir() {
    mkdir $1 && cd $1
}

function pydir() {
    mkdir $1 && cd $1
    touch __init__.py
}

function cl() {
    cd $1 && ls -la
}

function bucketclone() {
    git_pwicken_dir="$HOME/git/petewicken"
    if [[ $# == 1 ]]; then
        git clone git@bitbucket.org:petewicken/${1}.git "$git_pwicken_dir/$1"
        cd "$git_pwicken_dir/$1"
    else
        echo "Argument required."
    fi
}

function killit() {
    ps aux | grep -v "grep"| grep "$@" | awk '{print $2}' | xargs sudo kill
}

function proc() {
    ps aux | grep -v "grep" | grep -- "$@"
}

export PATH="$HOME/bin:/usr/local/bin:$PATH"
export SHELL=/bin/zsh
export EDITOR=vim

source ~/.commonrc/*
