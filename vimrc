set nocompatible              " be iMproved, required
filetype off                  " required


" ========= PLUGINS =========
call plug#begin('~/.vim/plugged')

" Plugin 'tpope/vim-fugitive'
" Plugin 'tpope/vim-surround'
" Plugin 'tpope/vim-repeat'
" Plugin 'tpope/vim-commentary'
" Plugin 'tpope/vim-obsession'
" 
" Plugin 'scrooloose/nerdtree'
" Plugin 'scrooloose/syntastic'
" 
" Plugin 'sirver/ack.vim'
" Plugin 'sirver/ultisnips'
" 
" Plugin 'vim-airline/vim-airline'
" Plugin 'vim-airline/vim-airline-themes'
" 
" Plugin 'honza/vim-snippets'
" Plugin 'yegappan/grep'
" Plugin 'ctrlpvim/ctrlp.vim'
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'benmills/vimux'
" Plugin 'ivanov/vim-ipython'
" Plugin 'plasticboy/vim-markdown'
" Plugin 'airblade/vim-gitgutter'
" Plugin 'davidhalter/jedi-vim'
" Plugin 'fs111/pydoc.vim'
" Plugin 'nathanaelkane/vim-indent-guides'
" Plugin 'bronson/vim-trailing-whitespace'
" Plugin 'christoomey/vim-tmux-navigator'
" Plugin 'terryma/vim-multiple-cursors'
" Plugin 'vitaly/vim-gitignore'
" Plugin 'jeffkreeftmeijer/vim-numbertoggle'
" Plugin 'raimondi/delimitmate'
" Plugin 'itchyny/vim-cursorword'
" Plugin 'rafi/vim-tagabana'
" Plugin 'majutsushi/tagbar'
" 
" " Themes
" Plugin 'nanotech/jellybeans.vim'
" Plugin 'altercation/vim-colors-solarized'

" ========= /PLUGINS =========

let mapleader = "\<space>"

nmap <leader>w :w<CR>

" SudoWrite command
try
    command SudoWrite w !sudo tee % > /dev/null
catch
endtry

" User Interface
set so=7
set wildmenu
set wildignore=*.o,*~,*.pyc
set ruler
set number
set autoread
set history=300
set cmdheight=1
set hid
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set ignorecase
set smartcase
set hlsearch
set incsearch
set showcmd
set diffopt+=vertical
set lazyredraw
set magic
set showmatch
set cursorline
set mat=2
set noerrorbells
set novisualbell
set t_vb=
set tm=500
set foldcolumn=1
set laststatus=2
set showtabline=2
set noshowmode

" Colors and Fonts
syntax enable

try
    colo solarized
catch
endtry

set background=dark
set encoding=utf8
set ffs=unix,dos,mac

" Files
set nobackup
set nowb
set noswapfile

func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

autocmd FileType yaml set shiftwidth=2
autocmd FileType yaml set tabstop=2

" Text and Indentation
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set lbr
set tw=500
set ai
set si
set wrap

nmap <silent> _ mz:m+<cr>`z
nmap <silent> + mz:m-2<cr>`z
nmap <silent> _ :m'>+<cr>`<my`>mzgv`yo`z
nmap <silent> + :m'>-2<cr>`>my`<mzgv`yo`z

map <leader>ss :setlocal spell! spelllang=en_gb<cr>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

map <silent> <leader>pp :setlocal paste!<cr>

" vnoremap > >gv
" vnoremap < <gv
" vnoremap <tab> >gv
" vnoremap <s-tab> <gv
" nnoremap <tab> >>_
" nnoremap <s-tab> <<_

" Searching
vnoremap <silent> # :call VisualSelection('f', '')<cr>
vnoremap <silent> * :call VisualSelection('b', '')<cr>

nnoremap & *N
map <silent> <leader><cr> :noh<cr>

vnoremap // y/<c-r>"<cr>

" Tabs and Buffers
map j gj
map k gk

map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-h> <c-w>h
map <c-l> <c-w>l

map <leader>bd :Bclose<cr>
map <leader>ba :1,100 bd!<cr>

map <leader>cd :cd %:p:h<cr>:pwd<cr>

try
    set switchbuf=useopen,usetab,newtab
    set stal=2
catch
endtry

autocmd BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \   exe "normal! g`\"" |
            \ endif
set viminfo^=%

" Navigation
map 0 ^

" imap <silent> <c-l> <c-o>a
" imap <silent> <c-h> <c-o>h
" imap <silent> <c-j> <c-o>j
" imap <silent> <c-k> <c-o>k

" Helper Functions
function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
   let l:currentBufNum = bufnr("%")
   let l:alternateBufNum = bufnr("#")

   if buflisted(l:alternateBufNum)
     buffer #
   else
     bnext
   endif

   if bufnr("%") == l:currentBufNum
     new
   endif

   if buflisted(l:currentBufNum)
     execute("bdelete! ".l:currentBufNum)
   endif
endfunction

" NerdTree
noremap <silent> <c-e> :NERDTreeToggle<cr>
let NERDTreeIgnore=['\.pyc$']
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" " Tagbar
autocmd VimEnter *.py call tagbar#autoopen(1)
map <silent> <c-x> :TagbarToggle<cr>
map <silent> <leader>z :TagbarClose<cr>:NERDTreeClose<cr>


" Grep
try
    map <leader>f :Rgrep -i<cr>
    map <leader>F :Rgrep<cr>
catch
endtry

" CTRL-P
let g:ctrlp_root_markers = ['.git']
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPLastMode'
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir']

" Multi-Cursor
let g:multi_cursor_next_key='<c-n>'
let g:multi_cursor_prev_key='<c-m>'
let g:multi_cursor_skip_key='<c-x>'
let g:multi_cursor_quit_key='<esc>'

" Syntastic
noremap <silent> <c-c> :SyntasticToggleMode<cr>

" Vimux
try
    nmap <silent> <leader>t :wa<cr>:VimuxRunLastCommand<cr>
catch
endtry

" ultisnips
let g:UltiSnipsExpandTrigger="<c-l>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" Airline Fonts
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
    endif

    " unicode symbols
    let g:airline_left_sep = '»'
    let g:airline_left_sep = '▶'
    let g:airline_right_sep = '«'
    let g:airline_right_sep = '◀'
    let g:airline_symbols.linenr = '␊'
    let g:airline_symbols.linenr = '␤'
    let g:airline_symbols.linenr = '¶'
    let g:airline_symbols.branch = '⎇'
    let g:airline_symbols.paste = 'ρ'
    let g:airline_symbols.paste = 'Þ'
    let g:airline_symbols.paste = '⎘'
    let g:airline_symbols.whitespace = 'Ξ'

    " airline symbols
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ''

" Miscellaneous
inoremap jj <ESC>
set mouse=a
set mousemodel=popup
map <leader>= gg=G

nmap <leader>a <c-a>
nmap <leader>x <c-x>

map < <<
map > >>

set guifont=Hack:h12
set guioptions-=L
set guioptions-=r
set guioptions-=T
set guioptions-=m
if !has('gui_macvim')
    " Compatibility for Terminal
    let g:solarized_termtrans=1

    if (&t_Co >= 256 || $TERM == 'xterm-256color')
        " Do nothing, it handles itself.
    else
        " Make Solarized use 16 colors for Terminal support
        let g:solarized_termcolors=16
    endif
endif

" set termguicolors=on

:noh
