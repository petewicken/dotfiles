#TODO: make this much nicer

DIR=$(shell pwd)

.PHONY: clean dotfiles fresh

clean:
	@-rm ~/.bash_logout
	@-rm ~/.bash_profile
	@-rm ~/.bashrc
	@-rm -r ~/.irssi
	@-rm ~/.tmux.conf
	@-rm ~/.vimrc
	@-rm ~/.zshrc

dotfiles: 
	@-ln -s $(DIR)/bash_logout ~/.bash_logout
	@-ln -s $(DIR)/bash_profile ~/.bash_profile
	@-ln -s $(DIR)/bashrc ~/.bashrc
	@-ln -s $(DIR)/irssi ~/.irssi
	@-ln -s $(DIR)/tmux.conf ~/.tmux.conf
	@-ln -s $(DIR)/vimrc ~/.vimrc
	@-ln -s $(DIR)/zshrc ~/.zshrc
	@echo Done.

fresh: clean dotfiles

