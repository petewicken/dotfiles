plugins=(zsh-autosuggestions, sudo, cpi, rsync, systemadmin, git, web-search, wd, tmux, celery, common-aliases, dirhistory, docker, git-extras, pip, python, django, docker-compose)

# Path to your oh-my-zsh configuration.
export ZSH=/Users/petewicken/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="spaceship"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set this to use case-sensitive completion
CASE_SENSITIVE="false"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often to auto-update? (in days)
export UPDATE_ZSH_DAYS=7

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to the command execution time stamp shown 
# in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"


# User configuration

export PATH="$PATH:/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/pete/bin"
# export MANPATH="/usr/local/man:$MANPATH"

export EDITOR='vim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"


### EXTERNAL ###

# Insert sudo at start of line using 'ALT+s'
insert_sudo () { zle beginning-of-line; zle -U "sudo " }
zle -N insert-sudo insert_sudo

setopt CORRECT

###### OWN STUFF ######

setopt appendhistory autocd no_beep

alias :q='exit'
alias vim='NVIM_LISTEN_ADDRESS="/tmp/nvim" nvim'
alias venv='source ~/venv/bin/activate'

export EDITOR=nvim

ctrlz () {
    if [[ $#BUFFER -eq 0 ]]; then
        fg
	zle redisplay
    else
        zle push-input
    fi
}

zle -N ctrlz
bindkey '^Z' ctrlz

bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
bindkey '^[[1~' beggining-of-line
bindkey '^[[2~' insert
bindkey '^[[3~' delete
bindkey '^[[3;5~' kill-word
bindkey '^[[4~' end-of-line
bindkey '^[[1;5C' forward-word

source $ZSH/oh-my-zsh.sh

#source ~/.gorc.sh
#source ~/.commonrc.sh


# source /usr/local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
